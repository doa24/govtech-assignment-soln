### Application Containerization : Build an Docker Image, Test the Image and Push to Dockerhub

#### Dockerfile for react application:
```shell
#Stage 1
FROM node:17-alpine as builder
WORKDIR /app
COPY package.json .
COPY package-lock.json .
RUN npm install
COPY . .
RUN npm run build

#Stage 2
FROM nginx:1.19.0
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=builder /app/build .
ENTRYPOINT ["nginx", "-g", "daemon off;"]

```

### Build a docker image using the Dockerfile and test it.
```shell
git clone https://gitlab.com/doa24/m-react-app.git
cd lb-ws/m-react-app/
docker build -t tanvir0102/m-react-app:latest .
docker run -p 3000:80 tanvir0102/m-react-app:latest

```

### Publish the docker image to Dockerhub
```shell
docker push tanvir0102/m-react-app:latest
docker pull tanvir0102/m-react-app:latest
```

## Let's run the application on Kubernetes.
### Deployment file for the m-react-application
```shell
apiVersion: apps/v1
kind: Deployment
metadata:
  name: m-react-app-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: m-react-app
  template:
    metadata:
      labels:
        app: m-react-app
    spec:
      containers:
        - name: m-react-app-container
          image: tanvir0102/m-react-app:latest
          ports:
            - containerPort: 80
```

### Service file for the m-react-application
```shell
apiVersion: v1
kind: Service
metadata:
  name: m-react-app-service
spec:
  selector:
    app: m-react-app
  ports:
    - protocol: TCP
      port: 80  
      targetPort: 80
  type: LoadBalancer 
```

### Commands used to run and test the application in a minikube setup
```
kubectl apply -f m-react-app-deploy.yaml
kubectl get po --watch
kubectl get po -o wide
kubectl get deployment
kubectl apply -f m-react-app-svc.yaml
kubectl get service

minikube service m-react-app-service
```
## Deloy a MySQL server to a Kubernetes

```
git clone https://gitlab.com/doa24/m-react-app.git
cd my-react-app/k8s-mysql

kubectl apply -f mysql-secret.yaml
kubectl apply -f mysql-storage.yaml
kubectl apply -f mysql-deployment.yaml

kubectl get pod
kubectl exec --stdin --tty mysql-85d5bb8d57-qr9cm -- /bin/bash
```

