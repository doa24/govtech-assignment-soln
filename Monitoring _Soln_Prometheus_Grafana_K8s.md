## Monitoring Solution Setup on Kubernetes using Prometheus & Graphana

![Alt text](image-15.png)

### Install Prometheus using Helm
```shell
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install prometheus prometheus-community/prometheus
helm install prometheus prometheus-community/prometheus --version=25.10.0
kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-ext
kubectl expose service prometheus-server --type=LoadBalancer --port=80 --target-port=9090 --name=prometheus-server-ext-lb

minikube service prometheus-server-ext
kubectl get po -w
kubectl get svc 
```

### Prometheus Dashboard after the Installation.

![Alt text](image-12.png)

### Install Grafana using Helm
```shell
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
helm install grafana grafana/grafana
kubectl expose service grafana --type=NodePort --target-port=3000 --name=grafana-ext
kubectl expose service grafana --type=LoadBalancer --target-port=3000 --name=grafana-ext-lb

minikube service grafana-ext
kubectl get po -w
kubectl get svc
kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo

kubectl expose service prometheus-kube-state-metrics --type=NodePort --target-port=8080 --name=prometheus-kube-state-metrics-ext
minikube service prometheus-kube-state-metrics-ext

helm list
helm repo list
helm uninstall
helm repo remove

```
### Kibana Dashboard after the Installation.
![Alt text](image-13.png)
![Alt text](image-14.png)

### Let's add Prometheus as Data Source.
### Import Below Dashboards to Grafana
3662 -  Prometheus 2.0 Overview

12740 - Kubernetes Monitoring Dashboard - A Dashboard to monitor the cluster

3119 - Kubernetes cluster monitoring (via Prometheus)

315 - Kubernetes cluster monitoring (via Prometheus)

### Install Prometheus MySQL Exporter
```shell

helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update

helm upgrade --install mysql-exporter prometheus-community/prometheus-mysql-exporter -f mysql-exporter-values.yaml

```