## Minikube setup and commad to start the minikube

```shell
minikube start --driver=virtualbox --no-vtx-check
minikube stop
minikube delete
```



## Challenges during the Prometheus setup using helm
### Error receive during prometheus setup
$ helm install prometheus prometheus-community/prometheus
Error: INSTALLATION FAILED: unable to build kubernetes objects from release manifest: error validating "": error validating data: ValidationError(StatefulSet.spec): unknown field "minReadySeconds" in io.k8s.api.apps.v1.StatefulSetSpec

helm install prometheus prometheus-community/prometheus --version=25.21.0
Error: INSTALLATION FAILED: unable to build kubernetes objects from release manifest: error validating "": error validating data: ValidationError(StatefulSet.spec): unknown field "minReadySeconds" in io.k8s.api.apps.v1.StatefulSetSpec

helm install prometheus prometheus-community/prometheus --version=25.10.0 | success | 22 Jan, 2024
### Troubleshooting steps

Kubernetes: v1.20.2 | Kubernetes version installed 

The error message you're seeing indicates that the minReadySeconds field is not recognized in the StatefulSet specification for the Kubernetes API version you are using. This issue typically arises when there are version incompatibilities or deprecated fields in the Helm chart being used.

You can specify a specific version of the chart that is known to be compatible with your Kubernetes version.

helm install prometheus prometheus-community/prometheus --version=25.10.0