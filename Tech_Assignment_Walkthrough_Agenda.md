### Technical Assignment Walkthrough Agenda...
### Assigment 01
- [ ] Explain the React Application coosen for the assignment.
- [ ] Explain the CI/CD setup for the m-react-app using Gitlab.
- [ ] Perform V1 Changes in develop branch and raise a merge request to merge in main branch
- [ ] Explian how connectivity/credentials setup between Gitlab repo and AWS Account.
- [ ] Check/Validate the data in S3 after the Build and Deploy stage completed.
- [ ] Explain the terraform code for setting up the Infra Structure for appliation
- [ ] Explain how modules are created and used, how config/variables are separated to multi environment.
- [ ] Once terraform Apply completed, check the application using ALB URL
- [ ] Perform the changes to Perform the V2 testing
- [ ] Check and Validate the Pipeline after the code merge to master branch
- [ ] Monitor the EC2 in the console and Launch time, Reload the ALB URL after 10mins.
- [ ] Don't Forget to clean the AWS Resources. 
    ```shell
    terraform -chdir=./environments/dev destroy
    ```
    
### Assignment 02:
- [ ] start minikube and download the Manifest files in local
- [ ] How application was containerized, Explain the Dockerfile
- [ ] Show the Dockercompose file that can be used by developer to perform local testing.
- [ ] Create deployment and Service for the appliation in local K8s setup and test the running application
- [ ] Deploy MySQL instance on K8 local setup and test the running my SQL instance
- [ ] Deploy Prometheus and Grafana and configure the Data Source as Prometheus 
- [ ] Create a Dashboard to monitor the the resources
