# Scenario 1 - Terraform, AWS, CICD:
To implement the problem statements/requirements given in scenario 1, I have used a simple react application to build and deploy the application to S3 Bucket and later it will be deployed/refreshed to EC2 Instance running in Auto Scaling group behind load balancer. 

## Architecture Diagram [ will be updated later]

![Alt text](AWS_Infra_Architecture.jpg)

## React Sample Application Code
[Reat Application Code can be found in the repo](https://gitlab.com/doa24/m-react-app)

## Gitlab Pipeline Code
[.gitlab-ci.yml - Stage 1 - Build the Application, Stage 2 - Deploy the build artifacts to S3 Bucket, Stage 3 - Perform start-instance-refresh](https://gitlab.com/doa24/m-react-app/-/blob/master/.gitlab-ci.yml)

## Terraform Code to Provision the required Infrastructure on AWS Cloud
[Terraform  code to provision network, Load balancer, Autoscaling group and required AWS resources to run the sample application](https://gitlab.com/doa24/counter-app-iac)

## Deployment Testing of Application Pipeline and Terraform Code

### Step 01: Make a small Change in appliation code in Develop branch, push the changes, raise a merge request and merge to the master branch. This will trigger the gilab pipeline to build and deploy the application to S3 Bucket.

![Alt text](image.png)

![Alt text](image-1.png)

### Step 02: Prepare the AWS infrastructure using terraform
#### Please setup the AWS IAM credentials and basic prerequisite before runnung the terraform plan or apply
```shell
git clone https://gitlab.com/doa24/counter-app-iac.git
cd counter-app-iac
terraform -chdir=./environments/dev init
terraform -chdir=./environments/dev plan
terraform -chdir=./environments/dev apply
```

### Step 03: Test the application using the ALB URL

![Alt text](image-2.png)

### Lets, Perform the second testing of application deployment to S3 as well instane refresh via Pipeline.
### Step 04: Make a Change in appliation code (Name in UPPER case and V2) in Develop branch, push the changes, raise a merge request and merge to the master branch. This will trigger the gilab pipeline to build and deploy the application to S3 Bucket and refresh the instance.

![Alt text](image-3.png)

![Alt text](image-4.png)

![Alt text](image-5.png)

![Alt text](image-6.png)

![Alt text](image-7.png)

![Alt text](image-8.png)

![Alt text](image-9.png)

![Alt text](image-11.png)

![Alt text](image-10.png)

### Let's Containerize the appliation using Docker and Write a Docker Compose file for Developer testing. 

[Dokerfile](https://gitlab.com/doa24/m-react-app/-/blob/master/Dockerfile?ref_type=heads)
```shell
#Stage 1
FROM node:17-alpine as builder
WORKDIR /app
COPY package.json .
COPY package-lock.json .
RUN npm install
COPY . .
RUN npm run build

#Stage 2
FROM nginx:1.19.0
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=builder /app/build .
ENTRYPOINT ["nginx", "-g", "daemon off;"]
```

[Docker Compose File](https://gitlab.com/doa24/m-react-app/-/blob/master/docker-compose.yml?ref_type=heads)
```shell
services:
  app:
    container_name: m-react-app-c
    image: m-react-app-i
    build: 
      context: .
    ports:
      - 3000:80
```

### Deliverable Checklist:
 ● Restrict inbound access to both public ALB and Server Fleet A to only allow on port 80/TCP [done]

 ● Install Nginx on Amazon Linux 2, Server Fleet A of EC2 [done]

 ● Able to view Incremental and Decremental Counter webpage from public internet  served by Server Fleet A (Screenshot required for both the original page and modified uppercase text page) [done]

 ● Describe in a README file, what further improvements to above architecture and setup would you recommend and why.

 ● Share the Gitlab pipeline with your interviewer [done]
 
 ● Push your terraform, web codes and gitlab-ci.yml file to your gitlab repo to be shared with your interviewer [done]

 ### Below improvement i have done in the Architecture diagram and terraform code
 - I have created 3 layer VPC network with 2 subnets(each in different AZs) in each layer to provide high availablity to the application, loadbalancer and database.
 - Only ALB running web layer exposed to public 
 - Created Data layer that can be used in future to deploy RDS instances.
 - In terraform code, i have create a network module to provision the VPC network to separate the code
 - I have structure the code for multiple environment by creating dev, stage, prod folder to separate the configuration


### Future Improvement
- We can use Terragrunt to manage or keep terraform configuration
- We can use Packer to create a Golden AMI, where we can pre bake required packages and config files.
- Later even we use Ansible to Configure EC2 instances instead of userdata.
- Write fine-grained IAM policy that will be assigned to Fedrated Identity.
- Currently we are exposing the application on 80 (HTTP), later we can add SSL certifiates and expose the application on 443 (HTTPS).

